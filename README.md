# 陳鍾誠的《計算機結構》課程

主題                | 內容
--------------------|--------------------------------------------
[0-nand2tetris](00)  | 
[1-布林邏輯閘](01)  | 
[2-算術單元](02)     | 
[3-記憶單元](03)  | 
[4-機器語言](04)         | 
[5-處理器](05)       | 
[6-組譯器](06)       | 

